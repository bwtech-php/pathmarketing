<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Categories
Route::get('categories', 'CategoryController@index');
Route::post('category/new', 'CategoryController@store');
Route::delete('category/delete/{id}', 'CategoryController@destroy');

//Variations
Route::get('variations', 'VariationController@index');
Route::post('variation/new', 'VariationController@store');
Route::delete('variation/delete/{id}', 'VariationController@destroy');

//Variation Values
Route::get('variationvalues', 'VariationValueController@index');
Route::post('variationvalue/new', 'VariationValueController@store');
Route::delete('variationvalue/delete/{id}', 'VariationValueController@destroy');

//Products
Route::get('newproductinfo', 'ProductController@newProductInfo');
Route::get('products', 'ProductController@index');
Route::post('product/new', 'ProductController@store');
Route::delete('product/delete/{id}', 'ProductController@destroy');
Route::post('product/edit', 'ProductController@edit');
Route::get('product', 'ProductController@show');

//Product Variations
Route::post('variation/product/add', 'VariationProductsController@addVariationToProduct');
Route::get('product/variations', 'ProductController@productVariations');

//Users
Route::get('users', 'HomeController@users');
Route::delete('user/delete/{id}', 'HomeController@deleteUsers');

//Frontend APIs

//Products
Route::get('productinfo', 'ProductController@productInfo');

//Orders
Route::get('orders', 'OrderController@show');

Route::delete('order/delete/{id}', 'OrderController@destroy');

Route::get('reviews', 'ReviewController@show');

//Sub Categories
Route::get('subcategories', 'SubCategoryController@show');

//Auth
Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');

Route::middleware('auth:sanctum')->get('/test', function (Request $request) {
    return Auth::user();
});

Route::middleware('auth:sanctum')->group(function() {
    Route::get('pin/verify', 'HomeController@verifyPinCode');
    Route::post('payment', 'PaymentController@payment');
    Route::get('orders/user', 'OrderController@userOrders');
    Route::post('order/new', 'OrderController@create');
    Route::post('order/new/paypal', 'OrderController@paypalOrder');
    
    //Live
    Route::get('order/shipment/pending', 'OrderController@pendingShipments');
    Route::get('payment/pending', 'PaymentController@pendingPayments');
    Route::get('orders/finished', 'OrderController@finishedOrders');
    Route::post('suggestion', 'HomeController@suggestion');
    Route::get('user/addresses', 'HomeController@customerAddresses');
    Route::post('user/addresses/new', 'HomeController@newAddress');
    Route::get('user/payment/methods', 'PaymentController@customerPaymentMethods'); //?stripe_id=
    Route::post('reviews/new', 'ReviewController@store');
    Route::post('user/payment/methods/new', 'PaymentController@newPaymentMethod');
    
    // Route::post('password/reset', 'Api\AuthController@resetPassword');
});

Route::get('filters', 'ProductController@ProductFilters');
Route::get('filters/results', 'ProductController@ProductFiltersResults');
Route::post('password/email', 'Api\ForgotPasswordController@forgot');
Route::get('support', 'HomeController@supportInfo');

Route::get('search', 'ProductController@search');
Route::get('subcategories/products', 'SubCategoryController@subCategoriesWithProducts');


Route::get('stripe', 'HomeController@stripe');

Route::get('dash', 'HomeController@dash');
Route::get('mergetest', 'HomeController@merging');

Route::get('test', 'OrderDetailsController@index'); //Testing123

Route::get('products/all', 'ProductController@productDetails');
Route::get('products/edit/options', 'ProductController@editOptions');

//Details
Route::get('details', 'DetailController@index');
Route::post('detail/new', 'DetailController@create');
Route::post('detail/edit', 'DetailController@edit');
Route::delete('detail/delete/{id}', 'DetailController@destroy');

//Detail values
Route::post('detail/value/new', 'DetailValueController@create');
Route::post('detail/value/edit', 'DetailValueController@edit');
Route::delete('detail/value/delete/{id}', 'DetailValueController@destroy');