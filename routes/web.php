<?php

use Illuminate\Support\Facades\Route;

//Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin');
    // return view('home');
})->middleware('auth');

Route::get('/home', function () {
    return view('admin');
    // return view('home');
})->middleware('auth');

Auth::routes();

Route::get('/admin', 'HomeController@admin')->middleware('auth');

Route::get('/test', function() {
    $user = App\User::find(1);
    // Mail::to($user)->send(new PinVerification($user));
    return new App\Mail\PinVerification($user);
    // return $user;
    // $test = '';
    // if($test == null) {
    //     return 1;
    // }
    // elseif($test != null) {
    //     return 0;
    // }
    // return rand(1000, 9999);
    // return $test;
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    return $exitCode;
});

Route::view('forgot_password', 'auth.reset_password')->name('password.reset');
Route::post('password/reset', 'Api\ForgotPasswordController@reset');