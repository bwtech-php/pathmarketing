<?php

namespace App\Http\Controllers;

use App\Variation_value;
use Illuminate\Http\Request;

class VariationValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Variation_value::with('variations')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $variationValue = new Variation_value;
        $variationValue->name = $request->variationValue;
        $variationValue->variation_id = $request->variation;
        $variationValue->price = $request->price;
        $variationValue->save();

        return response()->json($variationValue);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Variation_value  $variation_value
     * @return \Illuminate\Http\Response
     */
    public function show(Variation_value $variation_value)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Variation_value  $variation_value
     * @return \Illuminate\Http\Response
     */
    public function edit(Variation_value $variation_value)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Variation_value  $variation_value
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Variation_value $variation_value)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Variation_value  $variation_value
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $variationValue = Variation_value::findorfail($id);
        $variationValue->delete();

        return response()->json();
    }
}