<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $categories = Category::all();
            // $categories = $this->setData('data', $categories->toArray());
            // $data = ['categories' => $categories['data']];
            $data = ['categories' => $categories];
            return response()->json(['status' => true, 'message' => 'All the categories', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        global $categoryImage;
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $categoryImage = URL::to('/')."/images/categories/".$request->file('image')->getClientOriginalName();
                $path = $request->image->move(public_path('images/categories'), $request->file('image')->getClientOriginalName());
            }
        }
        
        $category = new Category;
        $category->name = $request->name;
        $category->image = $categoryImage;
        $category->save();

        return response()->json($category);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $category = Category::findorfail($id);
        $category->delete();

        return response()->json();
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
