<?php

namespace App\Http\Controllers;

use App\OrderDetails;
use App\Order;
use App\Variation_value;
use Illuminate\Http\Request;

class OrderDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = OrderDetails::findorfail(1)->get('variation_value_id');
        // return $order;
        // return json_decode( $order[0]);
        // return $order[0];
        $data = explode(",", $order[0]->variation_value_id);
        // return $data;

        $array = array();
        
        // return Variation_value::findorfail($data[1])->get('name');
        
        // foreach($data as $key) {
            // dd((int)$key);
            // print_r((int)$key . "\n");
            $array = OrderDetails::join('variation_values', 'order_details.variation_value_id', '=', 'variation_values.id')->where('variation_values.id', $key)->select('variation_values.name')->get();
            // return Variation_value::findorfail($key);
        // }
        // print_r($array);
return $array;
        // return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function show(OrderDetails $orderDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderDetails $orderDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderDetails $orderDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderDetails  $orderDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderDetails $orderDetails)
    {
        //
    }
}
