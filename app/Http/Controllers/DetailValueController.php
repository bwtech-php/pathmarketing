<?php

namespace App\Http\Controllers;

use App\DetailValue;
use Illuminate\Http\Request;

class DetailValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $detail = new DetailValue;
        $detail->name = $request->name;
        $detail->detail_id = $request->detail;
        $detail->save();

        return response()->json($detail);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetailValue  $detailValue
     * @return \Illuminate\Http\Response
     */
    public function show(DetailValue $detailValue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetailValue  $detailValue
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $detail = DetailValue::findorfail($request->id);
        $detail->name = $request->name;
        $detail->detail_id = $request->detail;
        $detail->save();

        return response()->json($detail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetailValue  $detailValue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetailValue $detailValue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetailValue  $detailValue
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail = DetailValue::findorfail($id);
        $detail->delete();

        return 1;
    }
}
