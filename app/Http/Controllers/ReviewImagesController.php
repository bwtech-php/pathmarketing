<?php

namespace App\Http\Controllers;

use App\ReviewImages;
use Illuminate\Http\Request;

class ReviewImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReviewImages  $reviewImages
     * @return \Illuminate\Http\Response
     */
    public function show(ReviewImages $reviewImages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReviewImages  $reviewImages
     * @return \Illuminate\Http\Response
     */
    public function edit(ReviewImages $reviewImages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReviewImages  $reviewImages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReviewImages $reviewImages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReviewImages  $reviewImages
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReviewImages $reviewImages)
    {
        //
    }
}
