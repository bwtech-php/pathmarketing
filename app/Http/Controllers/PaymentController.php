<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Order;
use App\User;
use Illuminate\Http\Request;
//test
class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }

    public function payment(Request $request) {
        
        try {
            //  $stripe = new \Stripe\StripeClient("sk_test_51HzrpsDUoUTP6Bs1k4X8aOLDZZEwboEbDzsjTR6n4SoARaM7BFeiuZzubp7Rx9jsYdhVcYRRuGsxmpaFIzvCBHe000hScStPVr");
             $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET', false));
        
            //Get Customer
            $customer =  $stripe->customers->retrieve(
                $request->stripe_customer_id,
                []
            );
            // return $customer;
            
            //Create Payment Method
            $paymentMethod =  $stripe->paymentMethods->create([
                'type' => 'card',
                'card' => [
                    'number' => $request->card_number,
                    'exp_month' => $request->exp_month,
                    'exp_year' => $request->exp_year,
                    'cvc' => $request->cvc,
                ],
            ]);
            // return $paymentMethod;

            //Attach Payment Method to Customer
            $customerPaymentMethod =  $stripe->paymentMethods->attach(
                $paymentMethod->id,
                ['customer' => $customer->id]
            );

            // return $customerPaymentMethod;

            //Create Payment Intent
            $paymentIntent = $stripe->paymentIntents->create([
                'amount' => $request->amount,
                'currency' => 'usd',
                'payment_method_types' => ['card'],
                'confirm' => true,
                'customer' => $customer->id,
                'payment_method' => $paymentMethod->id,
                'description' => 'Test Payment',
            ]);

            // $paymentDetails = new Payment;
            // $paymentDetails->user_id = Auth::id();
            // $paymentDetails->user_id

            // return $paymentIntent;

            //Payment Status check
            // $data = ['payment_details' => $paymentIntent->charges->data[0]->paid];
            
            $data = ['payment_details' => $paymentIntent];

            return response()->json(['status' => true, 'message' => 'Payment Successful!', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
        
        
    }

    public function pendingPayments(Request $request) {
        try {
            $pendingPayments = Order::where('user_id', \Auth::id())->where('payment_status', 0)->get();

            $pendingPayments = $this->setData('data', $pendingPayments->toArray());
            $data = ['pending_payments' => $pendingPayments['data']];  

            return response()->json(['status' => true, 'message' => 'Pending Payments', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function customerPaymentMethods(Request $request) {
        try {
            $paymentMethods = Payment::where('user_id', $request->user_id)->select('id', 'user_id', 'currency', 'card_number', 'exp_month', 'exp_year', 'created_at')->get();

            $paymentMethods = $this->setData('data', $paymentMethods->toArray());
            $data = ['payment_methods' => $paymentMethods['data']];  

            return response()->json(['status' => true, 'message' => 'Customer Payment Methods', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function newPaymentMethod(Request $request) {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET', false));
            
            //Get Customer
            $customer =  $stripe->customers->retrieve(
                $request->stripe_customer_id,
                []
            );
            // return $customer;
            
            //Create Payment Method
            $paymentMethod =  $stripe->paymentMethods->create([
                'type' => 'card',
                'card' => [
                    'number' => $request->card_number,
                    'exp_month' => $request->exp_month,
                    'exp_year' => $request->exp_year,
                    'cvc' => $request->cvc,
                ],
            ]);
            // return $paymentMethod;

            //Attach Payment Method to Customer
            $customerPaymentMethod =  $stripe->paymentMethods->attach(
                $paymentMethod->id,
                ['customer' => $customer->id]
            );

            // return $customerPaymentMethod;

            $paymentMethod = $this->setData('data', $paymentMethod->toArray());
            $data = ['payment_method' => $paymentMethod['data']];  

            return response()->json(['status' => true, 'message' => 'New Customer Payment Method', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
