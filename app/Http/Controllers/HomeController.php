<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Payment;
use App\Product;
use App\Order;
use App\Support;
use App\Suggestion;
use App\UserAddress;
use Auth;
use DB;
use Laravel\Cashier\Cashier;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function admin()
    {
        return view('admin');
    }

    public function users() {
        // return User::all();

        try {
            $users = User::with('addresses')->get();
            return response()->json(['status' => true, 'message' => 'All the Users', 'data' => $users]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function deleteUsers($id) {
        // $user = Product::find($id);
        // $user->delete();

        return User::find($id)->delete();

        return response()->json();
    }

    public function verifyPinCode(Request $request) {
        
        try {
            if($request->pin == Auth::user()->verification_pin) {
            $user = User::findorfail(Auth::id());
            $user->verified = 1;
            $user->save();
            $user = $this->setData('data', $user->toArray());
            $data = ['user' => $user['data']];
            return response()->json(['status' => true, 'message' => 'Pin successfully verified!', 'data' => $data]);
        }
        else {
            return response()->json(['status' => false, 'message' => 'Invalid pin! Please try again']);
        }
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
        
    }

    public function stripe(Request $request) {
        $stripeCustomer = Cashier::findBillable('cus_Ib5bMKKKSmaAWm');
        return $stripeCustomer->asStripeCustomer();
        $user = User::find(2);
        $stripeCustomer = $user->createAsStripeCustomer();
        return $stripeCustomer;
    }

    public function dash() {
        $totalEarnings = Payment::sum('amount');
        $orders = Order::count();
        $products = Product::count();
        $users = User::count();

        return response()->json(['totalEarnings' => $totalEarnings, 'orders' => $orders, 'products' => $products, 'users' => $users]);
    }

    public function supportInfo() {
        try {
            $support = Support::findorfail(1);

            $support = $this->setData('data', $support->toArray());
            $data = ['support' => $support['data']];
            return response()->json(['status' => true, 'message' => 'Support Information', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function suggestion(Request $request) {
        try {
            $suggestion = new Suggestion;
            $suggestion->user_id = Auth::id();
            $suggestion->subject = $request->subject;
            $suggestion->message = $request->message;
            $suggestion->save();

            $suggestion = $this->setData('data', $suggestion->toArray());
            $data = ['suggestion' => $suggestion['data']];
            return response()->json(['status' => true, 'message' => 'Suggestion saved successfully!', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function customerAddresses(Request $request) {
        try {
            $address = DB::table('user_addresses')
                        ->join('users', 'user_addresses.user_id', '=', 'users.id')
                        ->where('user_addresses.user_id', Auth::id())
                        ->select('user_addresses.*', 'users.name')
                        ->get();

            $address = $this->setData('data', $address->toArray());
            $data = ['address' => $address['data']];
            return response()->json(['status' => true, 'message' => 'User addresses', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function newAddress(Request $request) {
        try {
            $address = new UserAddress;
            $address->user_id = Auth::id();
            $address->address = $request->address;
            $address->save();

            $address = $this->setData('data', $address->toArray());
            $data = ['address' => $address['data']];
            return response()->json(['status' => true, 'message' => 'New User Address!', 'data' => $data]);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function merging() {
        $product = Product::all();
        // return $product;
        $order = Order::all();
        $product['CVB'] = 'SDSDDS';
        return $product;
        // $merged = $product->merge($order);
        return response()->json($merged->all());
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
