<?php

namespace App\Http\Controllers;

use App\Review;
use App\Order;
use App\OrderDetails;
use App\ReviewImages;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpKernel\Exception\HttpException;
class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            $review = new Review;
            $review->product_id = $request->product_id;
            $review->order_id = $request->order_id;
            $review->user_id = \Auth::id();
            $review->review = $request->review;
            $review->rating = $request->rating;
            $review->save();

            $orderDetails = OrderDetails::where('order_id', $request->order_id)->where('product_id', $request->product_id)->get();
           
            // return $orderDetails;
            foreach($orderDetails as $orderDetail) {
                $orderDetail->review_posted = 1;
                $orderDetail->save();
            }
            
            global $images;
            global $reviewImage;
            if($request->hasFile('image')) {
                foreach($request->file('image') as $image) {
                
                    if ($request->hasFile('image')) {
                        // if ($request->file('image')->isValid()) {
                            
                            $reviewImage = URL::to('/')."/images/reviewimages/".$image->getClientOriginalName();
                            $path = $image->move(public_path('images/reviewimages'), $image->getClientOriginalName());
                        // }
                    }
                    
                    $images = new ReviewImages;
                    $images->review_id = $review->id;
                    $images->image = $reviewImage;
                    $images->save();
                }
            }
            
            $review = $this->setData('data', $review->toArray());
            if($request->hasFile('image')) {
                $images = $this->setData('data', $images->toArray());
            }
            $data = ['review' => $review['data']];

            return response()->json(['status' => true, 'message' => 'New review added successfully!', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        // $reviews = Review::find(17);
        // $array = array($reviews);
        // $object = (object)$array;
        // return $object;
        // return $reviews->toJson();

        // if($reviews) {
        //     return response()->json(['status' => true, 'message' => 'All the Reviews', 'reviews' => $reviews]);
        // }
        // else {
        //     return response()->json(['status' => false, 'message' => 'No data found']);
        // }
        // $reviews = Review::findorfail('abc');
        // return $reviews;
        try {
            $reviews = Review::all();
            return response()->json(['status' => true, 'message' => 'All the Reviews', 'data' => $reviews]);
        } catch (\Exception $e) {
            
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
        return 1;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        //
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
