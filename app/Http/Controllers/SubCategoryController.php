<?php

namespace App\Http\Controllers;

use App\subCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\subCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(subCategory $subCategory)
    {
        try {
            $subCategories = subCategory::with('products.productImages')->get();
            $subCategories = $this->setData('data', $subCategories->toArray());
            $data = ['sub_categories' => $subCategories['data']];
            return response()->json(['status' => true, 'message' => 'All the sub categories with their products', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\subCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(subCategory $subCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\subCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, subCategory $subCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\subCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(subCategory $subCategory)
    {
        //
    }

    public function subCategoriesWithProducts(Request $request) {
        
        try {
            $subCategoriesWithProducts = subCategory::where('category_id', $request->id)->with('products')->get();

            $subCategoriesWithProducts = $this->setData('data', $subCategoriesWithProducts->toArray());
            $data = ['subCategoriesWithProducts' => $subCategoriesWithProducts['data']];
            return response()->json(['status' => true, 'message' => 'Sub Categories with their Products', 'data' => $data]);
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
        
        
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
