<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{
    public function forgot() {
        try {
            $credentials = request()->validate(['email' => 'required|email']);

            Password::sendResetLink($credentials);

            return response()->json(['status' => true, 'message' => 'Reset password link sent on your email id.']);
        
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function reset(ResetPasswordRequest $request) {
        try {
            $reset_password_status = Password::reset($request->validated(), function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
            });

            if ($reset_password_status == Password::INVALID_TOKEN) {
                // return response()->json(['status' => false, 'message' => 'Invalid Token! Please try again.']);
                return "Invalid Token! Please try again.";
            }

            // return response()->json(['status' => true, 'message' => 'Password has been successfully reset!']);
            return "Password has been successfully reset! Please log in to the app.";
        
        } catch (\Exception $e) {
            // return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
            return $e->getMessage();
        }
    }
}
