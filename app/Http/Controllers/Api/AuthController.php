<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Cashier\Cashier;

//Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\PinVerification;

class AuthController extends Controller
{
    
    public function register(Request $request) {
        
        try {
            
            // $stripe = new \Stripe\StripeClient(
            //   'sk_test_51HzrpsDUoUTP6Bs1k4X8aOLDZZEwboEbDzsjTR6n4SoARaM7BFeiuZzubp7Rx9jsYdhVcYRRuGsxmpaFIzvCBHe000hScStPVr'
            // );
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET', false));
            
           $validatedData = $request->validate([
            'user_name' => 'required|max:55',
            'email' => 'email|required',
            'password' => 'required'
            ]);

            if(User::where('email', $request->email)->exists()) {
                return response()->json(['status' => false, 'message' => 'User already exists! Please log in.']);
            }
            else {
                $user = User::create([
                'user_name' => $request->user_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'verification_pin' => rand(1000, 9999)
            ]);
            }

            $user = User::where('email', $request->email)->first();

            Mail::to($user)->send(new PinVerification($user));
            
            $accessToken =  $user->createToken('registration token');
            
            $stripeCustomer = $stripe->customers->create([
              'name' => $user->user_name,
              'email' => $user->email,
            ]);
            
            $user->stripe_id = $stripeCustomer->id; 
            $user->save(); 
            
            $user = $this->setData('user_data', $user->toArray());

            $data = ['user' => $user['user_data'], 'token' => $accessToken->plainTextToken];
            return response()->json(['status' => true, 'message' => 'You have registered successfully! Please check your email for the verification pin', 'data' => $data]); 
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
        
        
    }

    public function login(Request $request) {
        
        try {
            $request->validate([
                'email' => 'required|email',
                'password' => 'required',
                // 'device_name' => 'required',
            ]);

        $user = User::where('email', $request->email)->first();

         if (! $user || ! Hash::check($request->password, $user->password)) {
            // throw ValidationException::withMessages([
            // 'email' => ['The provided credentials are incorrect.'],
            //  ]);
             return response()->json(['status' => false, 'message' => 'The provided credentials are incorrect.']);
        }

        $token = $user->createToken('login token');
        $user = $this->setData('user_data', $user->toArray());
        $data = ['user' => $user['user_data'], 'token' => $token->plainTextToken];
        return response()->json(['status' => true, 'message' => 'You have logged in successfully!', 'data' => $data]);
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        } 
        
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
