<?php

namespace App\Http\Controllers;

use App\Variation_Products;
use App\Variation;
use Illuminate\Http\Request;

class VariationProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Variation_Products  $variation_Products
     * @return \Illuminate\Http\Response
     */
    public function show(Variation_Products $variation_Products)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Variation_Products  $variation_Products
     * @return \Illuminate\Http\Response
     */
    public function edit(Variation_Products $variation_Products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Variation_Products  $variation_Products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Variation_Products $variation_Products)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Variation_Products  $variation_Products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Variation_Products $variation_Products)
    {
        //
    }

    public function addVariationToProduct(Request $request) {
        // return $request;
        $productVariation = new Variation_Products;
        $productVariation->variation_id = $request->id;
        $productVariation->product_id = $request->product;
        $productVariation->save();

        return response()->json($productVariation);
    }
}
