<?php

namespace App\Http\Controllers;

use App\Variation;
use App\Product;
use Illuminate\Http\Request;

class VariationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $variations = Variation::with('values')->get();

            // $variations = $this->setData('data', $variations->toArray());
            $data = ['variations' => $variations];  

            return response()->json(['status' => true, 'message' => 'All the Variations', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // return $request;
        $variation = new Variation;
        $variation->name = $request->name;
        $variation->save();

        return back()->with('status', 'Variation created successfully!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $variation = new Variation;
        $variation->name = $request->name;
        $variation->save();

        return response()->json($variation);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Variation  $variation
     * @return \Illuminate\Http\Response
     */
    public function show(Variation $variation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Variation  $variation
     * @return \Illuminate\Http\Response
     */
    public function edit(Variation $variation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Variation  $variation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Variation $variation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Variation  $variation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $variation = Variation::findorfail($id);
        $variation->delete();

        return response()->json();
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
