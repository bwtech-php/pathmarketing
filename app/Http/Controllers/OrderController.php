<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use App\OrderDetails;
use App\Payment;
use App\Variation_value;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class OrderController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {              
        try {
            // $stripe = new \Stripe\StripeClient("sk_test_51HzrpsDUoUTP6Bs1k4X8aOLDZZEwboEbDzsjTR6n4SoARaM7BFeiuZzubp7Rx9jsYdhVcYRRuGsxmpaFIzvCBHe000hScStPVr");
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET', false));
            
            //Get Customer
            $customer =  $stripe->customers->retrieve(
                Auth::user()->stripe_id,
                []
            );
            // return $customer;

            //Create Payment Method
            $paymentMethod =  $stripe->paymentMethods->create([
                'type' => 'card',
                'card' => [
                    'number' => $request->card_number,
                    'exp_month' => $request->exp_month,
                    'exp_year' => $request->exp_year,
                    'cvc' => $request->cvc,
                ],
            ]);
            // return $paymentMethod;

            //Attach Payment Method to Customer
            $customerPaymentMethod =  $stripe->paymentMethods->attach(
                $paymentMethod->id,
                ['customer' => $customer->id]
            );
            // return $customerPaymentMethod;

            //Create Payment Intent
            $paymentIntent = $stripe->paymentIntents->create([
                'amount' => $request->grandTotal*100,
                'currency' => 'usd',
                'payment_method_types' => ['card'],
                'confirm' => true,
                'customer' => $customer->id,
                'payment_method' => $paymentMethod->id,
                'description' => 'New Order',
            ]);

            if($paymentIntent->charges->data[0]->paid == true) {
                //Creating new order
                $order = new Order;
                $order->user_id = Auth::id();
                $order->subTotal = $request->subTotal;
                $order->grandTotal = $request->grandTotal;
                $order->address = $request->address;
                $order->payment_status = 1;
                $order->order_status = 1;
                $order->save();

                //Saving card details
                $cardDetails = new Payment;
                $cardDetails->user_id = Auth::id();
                $cardDetails->transaction_id = $paymentIntent->id;
                $cardDetails->order_id = $order->id;
                $cardDetails->amount = $request->grandTotal;
                $cardDetails->currency = $paymentIntent->charges->data[0]->currency;
                $cardDetails->card_number = $request->card_number;
                $cardDetails->exp_month = $request->exp_month;
                $cardDetails->exp_year = $request->exp_year;
                $cardDetails->cvc = Hash::make($request->cvc);
                $cardDetails->payment_status = 1;
                $cardDetails->save();

                // $productArray=json_decode($request->productArray);
                // $quantityArray=json_decode($request->quantityArray);
                // $variationValueArray=json_decode($request->variationValueArray);

                $productArray=$request->productArray;
                $quantityArray=$request->quantityArray;
                $variationValueArray=$request->variationValueArray;
                // dd($productArray);
                $orderDetailsResponse = array();
                foreach($productArray as $key=> $product) {
                    $orderDetails = new OrderDetails;
                    $orderDetails->order_id = $order->id;
                    
                    $orderDetails->product_id = $productArray[$key];
                    
                    $orderDetails->quantity = $quantityArray[$key];
                    $orderDetails->variation_value_id = $variationValueArray[$key];
                    $orderDetails->save();

                    array_push($orderDetailsResponse, $orderDetails);
                }
                
            }

            $paymentIntent = $this->setData('data', $paymentIntent->toArray());
            $data = ['order' => $order, 'order_details' => $orderDetailsResponse, 'payment_details' => $paymentIntent['data']];  

            return response()->json(['status' => true, 'message' => 'Payment Successful and order placed!', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function paypalOrder(Request $request) {
        try {
            //Creating new order
            $order = new Order;
            $order->user_id = Auth::id();
            $order->subTotal = $request->subTotal;
            $order->grandTotal = $request->grandTotal;
            $order->address = $request->address;
            $order->payment_status = 1;
            $order->order_status = 1;
            $order->save();

            //Saving card details
            $cardDetails = new Payment;
            $cardDetails->user_id = Auth::id();
            $cardDetails->pay_id = $request->pay_id;
            $cardDetails->order_id = $order->id;
            $cardDetails->amount = $request->grandTotal;
            $cardDetails->payment_status = 1;
            $cardDetails->save();

            // $productArray=json_decode($request->productArray);
            // $quantityArray=json_decode($request->quantityArray);
            // $variationValueArray=json_decode($request->variationValueArray);

            $productArray=$request->productArray;
            $quantityArray=$request->quantityArray;
            $variationValueArray=$request->variationValueArray;
            // dd($productArray);
            $orderDetailsResponse = array();
            foreach($productArray as $key=> $product) {
                $orderDetails = new OrderDetails;
                $orderDetails->order_id = $order->id;
                
                $orderDetails->product_id = $productArray[$key];
                
                $orderDetails->quantity = $quantityArray[$key];
                $orderDetails->variation_value_id = $variationValueArray[$key];
                $orderDetails->save();

                array_push($orderDetailsResponse, $orderDetails);
            }
                
            $data = ['order' => $order, 'order_details' => $orderDetailsResponse];  

            return response()->json(['status' => true, 'message' => 'Payment Successful and order placed!', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function userOrders(Request $request) {
        try {
            
            //Latest stable
            $orders = Order::with('orderDetails.orderProducts')
                            ->where('orders.user_id', $request->id)
                            ->get();

            //Current Live
            // $orders = Order::
            // join('order_details', 'orders.id', '=', 'order_details.order_id')
            // ->join('products', 'order_details.product_id', '=', 'products.id')
            // ->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.id')
            // // ->join('variation_values', 'order_details.variation_value_id', '=', 'variation_values.id')
            // // ->select('orders.*', 'order_details.*', 'products.*', 'variation_values.*')
            // ->select('orders.*', 'order_details.quantity', 'products.id as product_id', 'products.name', 'products.image', 'products.description', 'products.price', 'products.rating', 'sub_categories.name')
            // ->with('orderDetails.orderProducts')
            // // ->select('orders.*')
            // ->where('orders.user_id', $request->id)
            // // ->groupBy('order_details.order_id')
            // ->get();
            // return $orders;
            
            // $variationValues = explode(",", $orders[0]['orderDetails'][0]->variation_value_id);
            // return $variationValues;
            
            $users = User::all();
            
            foreach ($orders as $key => $order) {
                // $variationValues;
                foreach($orders[$key]['orderDetails'] as $k => $order_details)
                {
                    // return $order_details->variation_value_id;
                    $variationValues[] = explode(",", $order_details->variation_value_id);

                    
                }
                // return $variationValues;
                // return $orders[$key]['orderDetails'];
                // $variationValues = explode(",", $orders[$key]['orderDetails'][$key]->variation_value_id);
                foreach($variationValues as $variationValue) {
                    
                    $variationValueItem[] = Variation_value::findMany($variationValue);
                    // $variationValueArray[] = $variationValueItem;
                    // return $variationValue;
                }
                // return $variationValueItem;
                // return $variationValueArray;
                
                // return $orders;
                // array_push($orders, $variationValueArray);
                foreach($orders[$key]['orderDetails'] as $k => $order_details)
                {
                    foreach($orders[$key]['orderDetails'][$k]['orderProducts'] as $p_k => $op)
                    {
                        $orders[$key]['orderDetails'][$k]['orderProducts'][$p_k]['variation_values'] = $variationValueItem[0];
                    }
                    
                }

                
                // return $test;
                // return $orders[$key]['orderDetails'];
                // $orders[$key]['variation_values']=$users;
            }

            // return $orders;

            

            // array_push($orders, $users);

            // $orders = Order::all();

            $orders = $this->setData('data', $orders->toArray());
            $data = ['orders' => $orders['data']];  

            return response()->json(['status' => true, 'message' => 'User orders', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function pendingShipments(Request $request) {
        try {
            $pendingShipments = Order::where('user_id', Auth::id())->where('shipping_status', 0)->get();

            $pendingShipments = $this->setData('data', $pendingShipments->toArray());
            $data = ['pending_shipments' => $pendingShipments['data']];  

            return response()->json(['status' => true, 'message' => 'Pending Shipments', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    public function finishedOrders(Request $request) {
        try {
            // $finishedOrders = Order::where('user_id', Auth::id())->where('order_status', 1)->get();

            $orders = Order::
            join('order_details', 'orders.id', '=', 'order_details.order_id')
            ->join('products', 'order_details.product_id', '=', 'products.id')
            ->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.id')
            // ->join('variation_values', 'order_details.variation_value_id', '=', 'variation_values.id')
            // ->select('orders.*', 'order_details.*', 'products.*', 'variation_values.*')
            ->select('orders.*', 'order_details.quantity', 'products.id as product_id', 'products.name', 'products.image', 'products.description', 'products.price', 'products.rating', 'sub_categories.name')
            ->with('orderDetails.orderProducts')
            // ->select('orders.*')
            // ->where('orders.user_id', $request->id)
            ->where('orders.user_id', Auth::id())
            ->where('orders.order_status', 1)
            ->where('orders.payment_status', 1)
            ->where('orders.shipping_status', 1)
            // ->groupBy('order_details.order_id')
            ->get();

            // return $orders;

            foreach ($orders as $key => $order) {
                // $variationValues;
                foreach($orders[$key]['orderDetails'] as $k => $order_details)
                {
                    // return $order_details->variation_value_id;
                    $variationValues[] = explode(",", $order_details->variation_value_id);

                    
                }
                // return $variationValues;
                // return $orders[$key]['orderDetails'];
                // $variationValues = explode(",", $orders[$key]['orderDetails'][$key]->variation_value_id);
                foreach($variationValues as $variationValue) {
                    
                    $variationValueItem[] = Variation_value::findMany($variationValue);
                    // $variationValueArray[] = $variationValueItem;
                    // return $variationValue;
                }
                // return $variationValueItem;
                // return $variationValueArray;
                
                // return $orders;
                // array_push($orders, $variationValueArray);
                foreach($orders[$key]['orderDetails'] as $k => $order_details)
                {
                    foreach($orders[$key]['orderDetails'][$k]['orderProducts'] as $p_k => $op)
                    {
                        $orders[$key]['orderDetails'][$k]['orderProducts'][$p_k]['variation_values'] = $variationValueItem[0];
                    }
                    
                }

                
                // return $test;
                // return $orders[$key]['orderDetails'];
                // $orders[$key]['variation_values']=$users;
            }

            $orders = $this->setData('data', $orders->toArray());
            $data = ['finished_orders' => $orders['data']];  

            return response()->json(['status' => true, 'message' => 'Finished Orders', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
