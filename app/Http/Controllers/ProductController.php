<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductImages;
use App\Detail;
use App\Variation_Products;
use App\Category;
use App\subCategory;
use App\outlet;
use App\Variation;
use App\Variation_value;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $products = Product::join('categories', 'products.category_id', 'categories.id')
        //                     ->select('products.*', 'categories.name as category_name')
        //                     ->with('variations')
        //                     ->get();
        
        // $variations = Variation::all();

        try {
            // $products = Product::join('sub_categories', 'products.sub_category_id', 'sub_categories.id')
            //             ->select('products.*', 'sub_categories.name as sub_category_name')
            //             ->with('reviews', 'variations.variationvalues')
            //             ->get();

            $products = Product::join('sub_categories', 'products.sub_category_id', 'sub_categories.id')
                        ->select('products.*', 'sub_categories.name as sub_category_name')
                        ->with('productImages', 'reviews.reviewImages', 'variations.variationvalues', 'details.value')
                        ->get();
            
            $products = $this->setData('data', $products->toArray());
            $data = ['products' => $products['data']];


            return response()->json(['status' => true, 'message' => 'All the Products', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
        
        // return response()->json((['products' => $products, 'variations' => $variations]));
    }

    public function productDetails() {

        // $product = Product::find(2)->with('manyDetails.detailValues')->first();
        // $product = Product::with('manyDetails.detailValues', 'variations.variationvalues', 'subCategory.category', 'productImages', 'reviews')->get();
        
        
        $product = Product::with('manyDetails.value', 'variations.variationvalues', 'subCategory.category', 'productImages', 'reviews')->get();
        
        // dd($product[0]->variations[0]->variationvalues->values->name);
        // dd($product[0]->manydetails[0]->detailvalues->name);
        return $product;
        
        // $abc = array();
        // foreach($products as $key => $prod)
        // {
        //     $variationValuesId =  $prod->variations[$key]->variationvalues->pluck('id');

        //     $products = Variation_Products::where('product_id',$prod->id)->whereIn('variation_value_id',$variationValuesId)->get();
        //     // return $abc;
        //     // $filteredCollection = $abc->filter(function ($value) { return !is_null($value); });
        //     // $products['abcf'] = collect($abc)->filter()->toArray();
        //     // $prod->variations[$key]->variationvalues[$key]->id;
        //     // return $abc;
        // }
        
        return $products;
    }

    public function editOptions() {
        $variationValues = Variation_value::with('variations')->get();
        $details = Detail::with('values')->get();

        return response()->json(['variation_values' => $variationValues, 'details' => $details]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        
        global $productImage;
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $productImage = URL::to('/')."/images/products/".$request->file('image')->getClientOriginalName();
                $path = $request->image->move(public_path('images/products'), $request->file('image')->getClientOriginalName());
            }
        }
        
        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->desc;
        $product->price = $request->price; 
        $product->sub_category_id = $request->sub_category_id;
        $product->image = $productImage;
        $product->save();

        // $productVariation = new Variation_Products;
        // $productVariation->product_id = $product->id;
        // $productVariation->variation_id = $product->variation_id;
        // $productVariation->save();

        return response()->json($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        try {
            $products = Product::join('sub_categories', 'products.sub_category_id', 'sub_categories.id')
                                ->where('products.id', $request->id)
                                ->select('products.*', 'sub_categories.name as sub_category_name')
                                ->with('variations.variationvalues', 'details.value', 'productImages', 'reviews.reviewImages')
                                ->firstorfail();
            //                     // return $products;
            // $productDetails = $products->productDetails->pluck('detail');
            // foreach($productDetails as $productDetail)
            // {
            //     $products->productDetails[$productDetail] = $products->productDetails->where('detail',$productDetail)->toArray();   
            // }
             
            //  return $products;
             $products = $this->setData('data', $products->toArray());
            $data = ['prod_details' => $products['data']];


            return response()->json(['status' => true, 'message' => 'Product Details', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, Request $request)
    { 
        // return $request;
        
        //Variations Work
            // $var = json_decode($request->get('variations'));
            // dd($var[0]->variationId);
            // foreach($request->get('variations') as $e) {
            //     $t = json_decode($e);
            //     // dd($t);
            //     foreach($t as $ab) {
            //         return $ab->variationId;
            //     }
            // }
        
        // foreach($request->file('image') as $image) {
        //     if ($request->hasFile('image')) { 
        //         $reviewImage = URL::to('/')."/images/test/".$image->getClientOriginalName();
        //         $path = $image->move(public_path('images/test'), $image->getClientOriginalName());
        //     }
        // }
        
        try {
            
            global $productImage;
            if($request->has('image')) {
                $productImages = ProductImages::where('product_id', $request->id)->get();

                foreach($productImages as $productImage) {
                    $productImage->delete();
                }

                foreach($request->file('image') as $image) {
                    if ($request->hasFile('image')) { 
                        $productImage = URL::to('/')."/images/products/".$image->getClientOriginalName();
                        $path = $image->move(public_path('images/products'), $image->getClientOriginalName());
                    }

                    $newProuctImages = new ProductImages;
                    $newProuctImages->product_id = $request->id;
                    $newProuctImages->image = $productImage;
                    $newProuctImages->save();
                }
            }
            
            $p = Product::findorfail($request->id);

            // global $productImage;
            // if ($request->hasFile('image')) {
            //     if ($request->file('image')->isValid()) {
            //         $productImage = URL::to('/')."/images/products/".$request->file('image')->getClientOriginalName();
            //         $path = $request->image->move(public_path('images/products'), $request->file('image')->getClientOriginalName());
            //     }
            // }

            $p->name = $request->name;
            $p->description = $request->description;
            $p->price = $request->price;
            // $p->image = $productImage;
            $p->save();
            
            $p = $this->setData('data', $p->toArray());
            $data = ['products' => $p['data']];


            return response()->json(['status' => true, 'message' => 'Product Details', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return response()->json();
    }

    public function newProductInfo() {
        $categories = Category::all();
        $subCategories = subCategory::all();
        $variations = Variation::with('variationValues')->get();

        return response()->json(['categories' => $categories, 'variations' => $variations, 'sub_categories' => $subCategories]);
    }

    public function productVariations() {
        $product = Product::find(2)->with('variations')->get();

        return $product;
    }

    public function productInfo() {
        $products = Product::all();
        $categories = Category::all();
        $variations = Variation::with('variationValues')->get();
        $outlets = outlet::all();

        return response()->json([
            'products' => $products,
            'categories' => $categories,
            'variations' => $variations,
            'outlets' => $outlets
        ]);
    }

    public function search(Request $request) {
        
        
        try {
            $products = DB::table('products')
            ->join('sub_categories', 'sub_categories.id', '=', 'products.sub_category_id')
            ->where('products.name', 'LIKE', '%'.$request->search.'%')
            ->orwhere('products.description', 'LIKE', '%'.$request->search.'%')
            ->orwhere('products.price', 'LIKE', '%'.$request->search.'%')
            ->select('products.*', 'sub_categories.name as sub_category')
            ->get();

            $categories = DB::table('categories')
            ->join('sub_categories', 'sub_categories.category_id', '=', 'categories.id')
            ->where('categories.name', 'LIKE', '%'.$request->search.'%')
            ->orwhere('sub_categories.name', 'LIKE', '%'.$request->search.'%')
            ->select('categories.*', 'sub_categories.name as sub_category')
            ->get();

            $products = $this->setData('productdata', $products->toArray());
            $categories = $this->setData('categorydata', $categories->toArray());
            $data = ['products' => $products['productdata'], 'categories' => $categories['categorydata']];

            return response()->json(['status' => true, 'message' => 'Search Results', 'data' => $data]);
        } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        } 
    }

    public function ProductFilters() {
        try {
            // $filters = Detail::with('detailValues')->get();
            $brands = Detail::where('name', 'Brand')->with('detailValues')->firstorfail();
            $conditions = Detail::where('name', 'Condition')->with('detailValues')->firstorfail();
            $materials = Detail::where('name', 'Material')->with('detailValues')->firstorfail();
            
            $colours = Variation::where('name', 'Color')->with('variationValues')->firstorfail();
            $sizes = Variation::where('name', 'Size')->with('variationValues')->firstorfail();
            
            $categories = Category::all();

            // return $filters->last();
            // $filters->put(6, $categories);

            // return $filters;

            // $categories = ['categories' => $categories];
            // array_push($filters, $categories);
            // return $filters;
            
            // $filters = $this->setData('data', $filters->toArray());
            $brands = $this->setData('data', $brands->toArray());
            $conditions = $this->setData('data', $conditions->toArray());
            $materials = $this->setData('data', $materials->toArray());
            $categories = $this->setData('data', $categories->toArray());
            $colours = $this->setData('data', $colours->toArray());
            $sizes = $this->setData('data', $sizes->toArray());
            $data = ['categories' => $categories['data'], 'brands' => $brands['data'], 'conditions' => $conditions['data'], 'materials' => $materials['data'], 'colours' => $colours['data'], 'sizes' => $sizes['data']];
            // $data = ['filters' => $filters['data'], 'categories' => $categories['data']];

            return response()->json(['status' => true, 'message' => 'All Filters', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }
    
    public function ProductFiltersResults(Request $request) {
        try {
            $products = Product::filter()
            ->with('productImages', 'reviews.reviewImages', 'variations.variationvalues', 'productDetails')
                        //         ->with(array('productImages'=>function($query){
                        //     $query->select('id','image as dfdfdf');
                        // }))
                        // ->select('products.*', 'productImages.image', 'reviews.reviewImages.image as review_images', 'variations.variationvalues.name as variation_values', 'productDetails.detail_value as product_details')
                        // ->select('products.*')
                        ->get();

            // return response()->json($products, 200);
            
            $products = $this->setData('data', $products->toArray());
            $data = ['prod_details' => $products['data']];


            return response()->json(['status' => true, 'message' => 'Product Details', 'data' => $data]);
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage(), 'code' => $e->getCode()]);
        }
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }
}
