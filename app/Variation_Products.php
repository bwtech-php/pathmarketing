<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variation_Products extends Model
{
    public $timestamps = false;

    protected $table = 'variation__products';

    // public function getCreatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }
    // public function getUpdatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }

    public function values()
    {
        return $this->belongsTo(Variation_value::class, 'variation_value_id', 'id');
    }
    
    public function Variation()
    {
        return $this->belongsTo('App\Variation');
    }


}
