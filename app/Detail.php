<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    public function products()
    {
        return $this->belongsTo('App\Product');
    }

    public function value()
    {
        return $this->hasOne('App\DetailValue');
    }

    public function values()
    {
        return $this->hasMany('App\DetailValue');
    }
}
