<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variation_value extends Model
{
    public function variations()
    {
        return $this->belongsTo(Variation::class, 'variation_id', 'id');
    }

    

    // public function getCreatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }
    // public function getUpdatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }
}
