<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Carbon\Carbon;


class Product extends Model
{
    // public function variations()
    // {
    //     return $this->belongsToMany('App\Variation', 'variation__products');
    // }

    // public function getCreatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }
    // public function getUpdatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }

    public function subCategory()
    {
        return $this->hasOne(subCategory::class, 'id', 'sub_category_id');
    }
    
    public function manydetails() {
        return $this->belongsToMany(Detail::class, 'detail_products', 'product_id', 'detail_id');
    }

    public function manyVariations() {
        return $this->belongsToMany(Variation::class, 'variation__products', 'product_id', 'variation_id');
    }

    public function variationValues()
    {
        return $this->belongsToMany('App\Variation_value', 'variations');
    }

    public function reviews()
    {
        // return $this->hasMany('App\Review')->where('id', 2);
        return $this->hasMany('App\Review')->join('users', 'reviews.user_id', '=', 'users.id')->select('reviews.*', 'users.name as user_name', 'users.image as user_image');
    }

    public function variations()
    {
        return $this->hasMany('App\Variation', 'product_id');
    }

    public function productDetails()
    {
        return $this->hasMany('App\ProductDetails', 'product_id');
    }

    public function details()
    {
        return $this->hasMany('App\Detail');
    }

    public function productImages()
    {
        return $this->hasMany('App\ProductImages');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'foreign_key', 'other_key');
    }

    /*
        sub_category
        condition
        material
        colour
        brand
        size
        price range
    */
    public function scopeFilter($q) {
        if (request('sub_category') != 0) {
            $q->where('sub_categories.name', 'LIKE', '%' . request('sub_category') . '%')->join('sub_categories', 'products.sub_category_id', '=', 'sub_categories.id');
        }
        if (request('condition') != 0) {
            $q->where('product_details.detail_value', 'LIKE', '%' . request('condition') . '%')->where('product_details.detail', 'Condition')->join('product_details', 'products.id', '=', 'product_details.product_id');
        }
        if (request('material') != 0) {
            $q->where('product_details.detail_value', 'LIKE', '%' . request('material') . '%')->where('product_details.detail', 'Material')->join('product_details', 'products.id', '=', 'product_details.product_id');
        }
        if (request('colour') != 0) {
            $q->where('variation_values.name', 'LIKE', '%' . request('colour') . '%')->join('variations', 'variation_values.variation_id', '=', 'variations.id');
        }
        if (request('brand') != 0) {
            $q->where('product_details.detail_value', 'LIKE', '%' . request('brand') . '%')->where('product_details.detail', 'Brand')->join('product_details', 'products.id', '=', 'product_details.product_id');
        }
        if (request('size') != 0) {
            $q->where('variation_values.name', 'LIKE', '%' . request('size') . '%')->join('variations', 'variation_values.variation_id', '=', 'variations.id');
        }
        if (request('price_from') != 0 or request('price_to') != 0) {
            $q->whereBetween('price', [request('price_from'), request('price_to')]);
        }
        // if ($keyword != '') {
        //     $q->whereBetween('price', [5, 500]);
        // }
        return $q;
    }
}
