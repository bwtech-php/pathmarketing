<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function subCategories()
    {
        return $this->hasMany('App\subCategory', 'category_id');
    }

    // public function getCreatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }
    // public function getUpdatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }

    // public function comments()
    // {
    //     return $this->hasMany('App\Comment', 'foreign_key', 'local_key');
    // }

    // public function subCategories($id) {
    //     return Category::where('sub_id' , $id)->get();
    // }
}
