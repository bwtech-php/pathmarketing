<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetails extends Model
{
    public function products()
    {
        return $this->belongsTo('App\Product');
    }

    // public function getCreatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }
    // public function getUpdatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }
}
