<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
    // public function variationValues()
    // {
    //     return $this->belongsTo(Variation_Products::class, 'id', 'variation_id')->with('values');
    // }

    public function variationValues()
    {
        // return $this->belongsToMany(Product::class, 'variation__products', 'variation_id', 'product_id')->with('values');
        return $this->belongsToMany(Product::class, 'variation__products', 'variation_id', 'product_id')->join('variation_values', 'variation__products.variation_value_id', '=', 'variation_values.id')->select('variation_values.*');
    }

    public function values()
    {
        return $this->hasMany(Variation_value::class, 'variation_id');
    }

    // public function getCreatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }
    // public function getUpdatedAtAttribute($value){
    //     $date = \Carbon\Carbon::parse($value);
    //     return $date->format('Y-m-d g:i A');
    // }
}
