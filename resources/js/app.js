require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import VueContentPlaceholders from 'vue-content-placeholders';
import Swal from 'sweetalert2';

window.Swal = Swal;

Vue.use(VueRouter);
Vue.use(VueContentPlaceholders);
// Vue.use(VueSweetalert2);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('home-component', require('./components/thespot/Home.vue').default);
Vue.component('products', require('./components/thespot/ProductsPage.vue').default);

const routes = [
    {
        path: '/',
        component: require('./components/thespot/Home.vue').default,
        props: true
    },
    {
        path: '/products',
        component: require('./components/thespot/ProductsPage.vue').default,
        props: true
    },
]

 const router = new VueRouter({
     routes
 })

Vue.prototype.$host_url = "http://thespot.test/api";

const app = new Vue({
    el: '#app',
    router
});