require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import VModal from 'vue-js-modal';
import VueFormulate from '@braid/vue-formulate';
import VueContentPlaceholders from 'vue-content-placeholders';
import {
    BootstrapVue,
    BootstrapVueIcons
} from "bootstrap-vue";
import PulseLoader from 'vue-spinner/src/PulseLoader.vue';
import Swal from 'sweetalert2';
import Multiselect from 'vue-multiselect'

window.Swal = Swal;

// import Vuetify from "../plugins/vuetify"

// import "bootstrap/dist/css/bootstrap.css";
// import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(VueRouter);
Vue.use(VModal);
Vue.use(VueFormulate);
Vue.use(VueContentPlaceholders);
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(PulseLoader);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('dashboard-component', require('./components/admin/Dashboard.vue').default);
Vue.component('products', require('./components/admin/Products.vue').default);
Vue.component('new-product', require('./components/admin/NewProduct.vue').default);
Vue.component('variations', require('./components/admin/Variations.vue').default);
Vue.component('new-variation', require('./components/admin/NewVariation.vue').default);
Vue.component('categories', require('./components/admin/Categories.vue').default);
Vue.component('new-category', require('./components/admin/NewCategory.vue').default);
Vue.component('variation-values', require('./components/admin/VariationValues.vue').default);
Vue.component('new-variation-value', require('./components/admin/NewVariationValue.vue').default);
Vue.component('multiselect', Multiselect)

Vue.filter("formattedDateTime", function(dateTime) {
    return moment.utc(dateTime).format('LLL')
});

const routes = [{
        path: '/',
        component: require('./components/admin/Dashboard.vue').default,
        props: true
    },
    {
        path: '/example',
        component: require('./components/ExampleComponent.vue').default,
        props: true
    },
    {
        path: '/products',
        component: require('./components/admin/Products.vue').default,
        props: true
    },
    {
        path: '/product/new',
        component: require('./components/admin/NewProduct.vue').default,
        props: true
    },
    {
        path: '/variations',
        component: require('./components/admin/Variations.vue').default,
        props: true
    },
    {
        path: '/variation/new',
        component: require('./components/admin/NewVariation.vue').default,
        props: true
    },
    {
        path: '/categories',
        component: require('./components/admin/Categories.vue').default,
        props: true
    },
    {
        path: '/category/new',
        component: require('./components/admin/NewCategory.vue').default,
        props: true
    },
    {
        path: '/variationvalues',
        component: require('./components/admin/VariationValues.vue').default,
        props: true
    },
    // { path: '/variationsvalues/new', component: require('./components/admin/NewVariationValue.vue').default, props: true },
    {
        path: '/users',
        component: require('./components/admin/Users.vue').default,
        props: true
    },
    {
        path: '/orders',
        component: require('./components/admin/Orders.vue').default,
        props: true
    },
    {
        path: '/subcategories',
        component: require('./components/admin/SubCategories.vue').default,
        props: true
    },
    {
        path: '/details',
        component: require('./components/admin/Details.vue').default,
        props: true
    },
]

const router = new VueRouter({
    routes
})

Vue.prototype.$host_url = "api";

const app = new Vue({
    el: '#app',
    router,
    // vuetify: Vuetify
});