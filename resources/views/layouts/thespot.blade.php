<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Bootstrap CSS -->
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('thespotfavicon.png') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="{{ asset('thespotassets/slick-slider/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('thespotassets/slick-slider/slick/slick.css') }}">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('thespotassets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('thespotassets/css/responsive.css') }}">
    {{-- <title>{{ config('app.name', 'Path Marketing') }}</title> --}}
    <title>Path Marketing</title>
  </head>
  <body class="inner-pages">
    <div id="app">
        <header>
        <div class="menu-area">
            <div class="container">
            <div class="row">
                <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                <div class="logo-area">
                    <a class="" href="/"><img src="{{ asset('thespotassets/images/logo.png') }}"  class="img-fluid" alt="logo"></a>
                    <div class="navbar-expand-lg navbar-light d-inline">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    </div>
                </div>
                </div>
                <div class="col-12 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                <div class="menu-bar">
                    <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="collapse navbar-collapse justify-content-center" id="navbarText">
                        <ul class="navbar-nav ml-auto">
                        <li class="active"><router-link to="/">Home</router-link></li>
                        <li><a href="/about">About</a></li>
                        <li><router-link to="/products">Products</router-link></li>
                        <li><a href="/flavors">Flavors</a></li>
                        <li><a href="/contact">Contact</a></li>
                        </ul>
                    </div>
                    <div class="cart-icons">
                        <ul class="list-unstyled">
                        <li class="list-inline-item"><a href="#"><i class="fas fa-shopping-cart"></i></a></li>
                        </ul>
                    </div>
                    </nav>
                </div>
                </div>
            </div>
            </div>
        </div>
        </header>
        <div>
            <section>
                @yield('content')
            </section>
        </div>
        {{-- <section class="main-banner">
        <div class="container">
            <h1>Welcome To The Spot</h1>
            <p>A snoball is a sweet dessert made of finely shaved ice. This summertime treat got its start in New Orleans in the 1930s.</p>
            <div class="button">
            <a href="#" class="btn btn-yellow">Order Now <i class="fas fa-arrow-right"></i></a>
            </div>
        </div>
        </section>
        <section class="main-about-us">
        <div class="container">
            <div class="row align-items-center">
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                <div class="img-box">
                <img src="{{ asset('thespotassets/images/about-img.jpg') }}"  class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                <div class="text-caption">
                <h2 class="sec-heading">About Us</h2>
                <p>THE SPOT – SNO BALLS & MORE is Texas premium snowball company, offering fluffy ice desserts to the greater TX community. Today, THE SPOT offers multiple classic flavors, including familiar classics like Cherry and Blue Razz and exotic flavors like Mango. </p>
                <p>THE SPOT is available for a variety of events, from private parties to corporate get-togethers and schoolyard festivities.</p>
                <p>THE SPOT prides itself on customer service and being a sweet addition in your Life! <strong>Stay Cool & Keep Shavin'</strong></p>
                <div class="button">
                    <a href="#" class="btn btn-yellow">Read More</a>
                </div>
                </div>
            </div>
            </div>
        </div>
        </section>
        <section class="main-menu">
        <div class="container">
            <div class="inner-menu">
            <div class="menu-main-title text-center">
                <h2>Flavors</h2>
            </div>
            <div class="pricing-bar">
                <ul class="list-unstyled">
                <li class="list-inline-item">Small $2.50</li>
                <li class="list-inline-item">Med $3.50</li>
                <li class="list-inline-item">large $4.50</li>
                <li class="list-inline-item">Jumbo $5.50</li>
                <li class="list-inline-item">Spot Size $6.50</li>
                </ul>
            </div>
            <div class="menu-lists">
                <div class="row">
                <div class="col-12 col-sm-3 col-md-3 col-lg-3 p-0">
                    <div class="menu-list-item">
                    <ul class="list-unstyled">
                        <li>Amaretto</li>
                        <li>Bahama Mama</li>
                        <li>Black Berry</li>
                        <li>Blue Berry</li>
                        <li>Blue Coton Candy</li>
                        <li>Blue Bubble gum</li>
                        <li>Banana</li>
                        <li>Blue Hawaiin</li>
                        <li>Candy Apple</li>
                        <li>Cherry</li>
                        <li>Coconout</li>
                        <li>Cola</li>
                        <li>Cotton Candy</li>
                        <li>Cream Soda</li>
                        <li>Egg Custord</li>
                    </ul>
                    </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3 col-lg-3 p-0">
                    <div class="menu-list-item">
                    <ul class="list-unstyled">
                        <li>Cantaloupe</li>
                        <li>Cake Batter</li>
                        <li>Chocolate</li>
                        <li>Dreamsicle</li>
                        <li>Dulce de Leche</li>
                        <li>Graphe</li>
                        <li>Grape Fruit</li>
                        <li>Georgia Peach</li>
                        <li>Granny Smith Apple</li>
                        <li>Hawaiin</li>
                        <li>Horchata</li>
                        <li>Hurricane</li>
                        <li>Icecream</li>
                        <li>kiwi</li>
                        <li>Lemon</li>
                    </ul>
                    </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3 col-lg-3 p-0">
                    <div class="menu-list-item">
                    <ul class="list-unstyled">
                        <li>Lemon Lime</li>
                        <li>Leche</li>
                        <li>Mango</li>
                        <li>Mocha</li>
                        <li>Margarita</li>
                        <li>Nector</li>
                        <li>Orange</li>
                        <li>Pine Colada</li>
                        <li>Pineapple</li>
                        <li>Pink Bubble Gum</li>
                        <li>Pink Lemonade</li>
                        <li>Praline</li>
                        <li>Rasberry</li>
                        <li>Red Velbet Cake</li>
                        <li>Root Beer</li>
                    </ul>
                    </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3 col-lg-3 p-0">
                    <div class="menu-list-item">
                    <ul class="list-unstyled">
                        <li>Sanfria</li>
                        <li>Strawberry</li>
                        <li>Strawberry Cheese Cake</li>
                        <li>Spearmint</li>
                        <li>Tamarindo</li>
                        <li>Tigers Blood</li>
                        <li>Tutti Frutti</li>
                        <li>Vanilla</li>
                        <li>Watermelon</li>
                        <li>Wedding Cake</li>
                        <li>White Chocolate Chip</li>
                        <li>Snosour</li>
                    </ul>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="button text-center">
            <a href="#" class="btn btn-yellow">Order Now</a>
            </div>
        </div>
        </section>
        <section class="main-what-people-say">
        <div class="container">
            <div class="inner-what-people-say">
            <div class="what-people-say-title text-center">
                <h2 class="sec-heading">What People Say</h2>
            </div>
            <div id="testimonials">
                <div class="item-testimon">
                <div class="img-box">
                    <img src="{{ asset('thespotassets/images/testimonial.png') }}"  class="img-fluid d-block mx-auto" alt="">
                </div>
                <div class="text-caption text-center">
                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                </div>
                </div>
                <div class="item-testimon">
                <div class="img-box">
                    <img src="{{ asset('thespotassets/images/testimonial.png') }}" class="img-fluid d-block mx-auto" alt="">
                </div>
                <div class="text-caption text-center">
                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                </div>
                </div>
                <div class="item-testimon">
                <div class="img-box">
                    <img src="{{ asset('thespotassets/images/testimonial.png') }}" class="img-fluid d-block mx-auto" alt="">
                </div>
                <div class="text-caption text-center">
                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                </div>
                </div>
            </div>
            </div>
        </div>
        </section> --}}
        <footer>
        <div class="main-footer">
            <div class="container">
            <div class="row">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2"></div>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <div class="subcriber-title text-center">
                    <h2 class="sec-heading">Subscribe Us</h2>
                </div>
                <div class="text-caption">
                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                </div>
                <div class="subscribe-form text-center">
                    <form action="">
                    <div class="form-group">
                        <input type="email" name="email" id="email" placeholder="Enter Your Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <div class="button">
                        <button type="submit" class="btn btn-yellow">Subscribe</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="footer-social-icons text-center">
                    <ul class="list-unstyled">
                    <li class="list-inline-item facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="list-inline-item twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li class="list-inline-item pinterest"><a href="#"><i class="fab fa-pinterest"></i></a></li>
                    <li class="list-inline-item linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </div>
                </div>
                
                <div class="col-2 col-sm-2 col-md-2 col-lg-2"></div>
            </div>
            </div>
        </div>
        <div class="container">
            <div class="copyright">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                <p>Copyright ©2020 The Spot . All Rights Reserved. </p>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 text-right">
                <div class="footer-cards">
                    <img src="{{ asset('thespotassets/images/cards.png') }}"  class="img-fluid" alt="">
                </div>
                </div>
            </div>
            </div>
        </div>
        </footer>
    </div>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- Optional JavaScript -->
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
            <script src="{{ asset('thespotassets/slick-slider/slick/slick.min.js') }}"></script>

        <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
            <script>
            jQuery(document).ready(function($) {
            $("#testimonials").slick({
            // vertical: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            draggable: true,
            centerPadding: '0px',
            verticalSwiping: true,
            autoplay: false,
            autoplaySpeed: 2000,
            dots: true,
            arrows: false,
            responsive: [{
                breakpoint: 992,
                settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            }]
            });

            $('#timepicker').timepicker();
            });
            </script>
            <script>
                
            </script>
</body>
</html>