{{-- @component('mail::message')
# Introduction

The body of your message.
{{ $ticket->title }} has been closed!

@component('mail::button', ['url' => 'www.google.com'])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent --}}


<table data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td style="padding:29px 0 30px;">
											<a target="_blank" href="{{ url('/') }}"><img src="https://i.imgur.com/UmWSfsY.png" width="250" height="100" alt=""></a>
											{{-- <div style="float: right; margin-top: 35px" class="nav" data-color="text" data-size="size navigation" data-min="10" data-max="22">
												<a target="_blank" href="{{ url('/') }}">Home</a> &nbsp; &nbsp; <a target="_blank" href="{{ url('/about') }}">About</a> &nbsp; &nbsp; <a target="_blank" href="{{ url('/contact-us') }}">Contact</a>
											</div> --}}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
<table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td class="img-flex"><img src="https://pathmarketing.co/wp-content/uploads/2020/09/brand-slide-1.png" style="vertical-align:top;" width="600" height="306" alt="" /></td>
									</tr>
									<tr>
										<td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">
														Verification Code
													</td>
												</tr>
												<tr>
													<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">
                                                        Thank you for registering with us! Your verification pin code is: <strong>{{ $user->verification_pin }}</strong>. Please use it in the application to continue.
{{-- <img src="https://i.imgur.com/eA1Kqk2.png" alt="">
<img src="https://i.imgur.com/bWDl5aL.jpg" alt=""> --}}
													</td>
												</tr>
												{{-- <tr>
													<td style="padding:0 0 20px;">
														<table width="134" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
															<tr>
																<td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f">
																	<a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="{{ url('/tickets') }}">Tickets</a>
																</td>
															</tr>
														</table>
													</td>
												</tr> --}}
											</table>
										</td>
									</tr>
									<tr><td height="28"></td></tr>
								</table>
							</td>
						</tr>
                    </table>