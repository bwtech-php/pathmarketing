@extends('layouts.thespot')

@section('content')
<router-view auth="{{ Auth::user() }}"></router-view>
@endsection