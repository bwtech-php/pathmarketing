@extends('layouts.admin')

@section('content')
<router-view auth="{{ Auth::user() }}"></router-view>
@endsection